pragma solidity >=0.4.0 <0.6.0;

import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol';
import '../node_modules/openzeppelin-solidity/contracts/lifecycle/Pausable.sol';
import '../node_modules/openzeppelin-solidity/contracts/lifecycle/Destructible.sol';
import '../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol';
import '../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol';

/**
* @title Events
* @dev It is a main contract that provides ability to create events, view information about events and buy tickets.
*/
contract Events is Pausable, Destructible ,ERC721Token("EventTicket", "🎟️") {
	using SafeMath for uint;

//Address of the deployed ERC20 tokens used for buying event tickets
	address tokenAddress;

	struct Event {
		address owner;
		string name;
		uint time;
		uint price;
		bool limited;
		uint seats;
		uint sold;
	}
	
	struct TicketSale {
	    address owner;
		uint eventid;
		uint ticketid;
		uint price;
		uint seat;
		bytes32 avail;
	}
	
	struct SecondaryTicketSale {
	    address owner;
		uint eventid;
		uint ticketid;
		uint price;
		uint seat;
		bytes32 avail;
	}
	

	struct Ticket {
	    address owner;
		uint event_id;
		uint ticket_id;
		uint seat;
		uint price;
		bytes32 avail;
	}

	Ticket[] internal tickets;
	Event[] private events;
	TicketSale[] private ticketsale;
	SecondaryTicketSale[] private secondaryticketsale;
	mapping(address => uint256[]) private ownedEvents;
	
	
	uint ticketid = 0;
	uint ssticketid = 0;
	uint totalTicketsSold = 0; //Tickets sold individual + tickets sold for Secondary sales -- getTotalSoldTickets
	uint totalResaleTicketsSold = 0; // Tickets sold by first buyers and pruchased by second buyers
	uint totalSecondarySaleTickets = 0; //Tickets bought by secondary sales agent
	uint totalSecondaryTicketsSold = 0; //Tickets sold in secondary sales market -- getTotalSSSoldTickets
	uint totalResaleTickets = 0; //Number of tickets available for Resale

    //To get the owner of a particular ticket number
	function ticketsOf(address _owner) public view returns(uint[] memory) {
		return ownedTokens[_owner];
	}

	//To get the details of a specified ticket id
	function getTicket(uint _id) public view returns(uint,uint, uint, uint, address) {
		require(_id < tickets.length);
		Ticket memory _ticket = tickets[_id];
		return(_ticket.event_id,_ticket.ticket_id, _ticket.seat, _ticket.price,_ticket.owner);
	}
	
	//Token address is passed during Event contract deployment
	constructor(address _token) public {
		tokenAddress = _token;
	}

	//To check if event time is in future
    modifier goodTime(uint _time) {
        require(_time > now);
        _;
    }
	
	//To check if the event exists
    modifier eventExist(uint _id) {
        require(_id < events.length);
        _;
    }

    function getTotalSoldTickets() public view returns(uint) {
		return totalTicketsSold;
	}
	
	function getTotalSSSoldTickets() public view returns(uint) {
		return(totalSecondaryTicketsSold);
	}
	
	function getOwnerSSTicket(uint _ticketID) public view returns(address) {
	SecondaryTicketSale memory _saleTkt = secondaryticketsale[_ticketID];
		return _saleTkt.owner;
		
	}

	function createEvent(
		string memory _name,
		uint _time,
		uint _price,
		bool _limited,
		uint _seats
	)
		goodTime(_time)
		whenNotPaused()
		public
	{
		Event memory _event = Event({
			owner: msg.sender,
			name: _name,
			time: _time,
			price: _price,
			limited: _limited,
			seats: _seats,
			sold: 0
		});
		uint _eventId = events.push(_event).sub(1);
		ownedEvents[msg.sender].push(_eventId);
	}


    function SellTicket(
		uint _eventid,
		uint _ticketid,
		uint _price
	)
		public
	{

	    Ticket memory _ticket = tickets[_ticketid];
	    
		TicketSale memory _saleTkt1 = TicketSale({
		    owner: msg.sender,
			eventid: _eventid,
			ticketid: _ticketid,
			price: _price,
			seat: _ticket.seat,
			avail : "Y"
		});
		
		require( _price <= _ticket.price*110/100,"You cannot sale more than 110 percent of your purchase value of ticket");
		
		require(msg.sender == _ticket.owner,"You are not the owner");

		ticketsale.push(_saleTkt1);
		totalResaleTickets++;
	}
	
	function getSecondarySaleTicket(uint _ticket_id) public view returns(uint, uint,uint,uint,address) {
		require(_ticket_id < secondaryticketsale.length);
		SecondaryTicketSale memory _saleTkt = secondaryticketsale[_ticket_id];
		return(_saleTkt.eventid,_saleTkt.ticketid,_saleTkt.seat,_saleTkt.price,_saleTkt.owner);
	}
	
	function getAllSecondarySaleTicket() public view returns (uint[] memory, uint[] memory,uint[] memory,uint[] memory ,address[] memory, bytes32[] memory){
      uint[]  memory eventid = new uint[](totalSecondarySaleTickets);
      uint[]  memory ticket_id = new uint[](totalSecondarySaleTickets);
      uint[]  memory seat = new uint[](totalSecondarySaleTickets);
      uint[]  memory price = new uint[](totalSecondarySaleTickets);
      address[]  memory owner = new address[](totalSecondarySaleTickets);
      bytes32[]  memory avail = new bytes32[](totalSecondarySaleTickets);
      for (uint i = 0; i < totalSecondarySaleTickets; i++) {
          SecondaryTicketSale memory ss = secondaryticketsale[i];
          eventid[i] = ss.eventid;
          ticket_id[i] = ss.ticketid;
          seat[i] = ss.seat;
          price[i]=ss.price;
          owner[i]=ss.owner;
          avail[i]=ss.avail;
      }
	return (eventid, ticket_id,seat,price,owner,avail);
  }
  
    function getAllAvailableTickets() public view returns (uint[] memory, uint[] memory,uint[] memory,uint[] memory ,address[] memory, bytes32[] memory){
      uint[]  memory eventid = new uint[](totalTicketsSold);
      uint[]  memory ticket_id = new uint[](totalTicketsSold);
      uint[]  memory seat = new uint[](totalTicketsSold);
      uint[]  memory price = new uint[](totalTicketsSold);
      address[]  memory owner = new address[](totalTicketsSold);
      bytes32[]  memory avail = new bytes32[](totalTicketsSold);
      for (uint i = 0; i < totalTicketsSold; i++) {
          Ticket memory tkt = tickets[i];
          eventid[i] = tkt.event_id;
          ticket_id[i] = tkt.ticket_id;
          seat[i] = tkt.seat;
          price[i]=tkt.price;
          owner[i]=tkt.owner;
          avail[i]=tkt.avail;
      }
	return (eventid, ticket_id,seat,price,owner,avail);
  }
  
   function getAllResaleTickets() public view returns (uint[] memory, uint[] memory,uint[] memory,uint[] memory ,address[] memory,bytes32[] memory){
      uint[]  memory eventid = new uint[](totalResaleTickets);
      uint[]  memory ticket_id = new    uint[](totalResaleTickets);
      uint[]  memory seat = new uint[](totalResaleTickets);
      uint[]  memory price = new uint[](totalResaleTickets);
      address[]  memory owner = new address[](totalResaleTickets);
      bytes32[]  memory avail = new bytes32[](totalResaleTickets);
      for (uint i = 0; i < totalResaleTickets; i++) {
          TicketSale memory _saleTkt = ticketsale[i];
          eventid[i] = _saleTkt.eventid;
          ticket_id[i] = _saleTkt.ticketid;
          seat[i] = _saleTkt.seat;
          price[i]=_saleTkt.price;
          owner[i]=_saleTkt.owner;
          avail[i]=_saleTkt.avail;
      }
	return (eventid, ticket_id,seat,price,owner,avail);
  }
	
	function updateSSTicketPrice(uint _ticket_id,uint price) public returns(bool) {
		
		SecondaryTicketSale memory _saleTkt = secondaryticketsale[_ticket_id];
		
		require(msg.sender == _saleTkt.owner,"You are not the owner");
	
	   	SecondaryTicketSale memory _saleTkt1 = SecondaryTicketSale({
		    owner: _saleTkt.owner,
			eventid: _saleTkt.eventid,
			ticketid: _saleTkt.ticketid,
			price: price,
			seat: _saleTkt.seat,
			avail: "Y"
		});
		
		secondaryticketsale[_ticket_id] = _saleTkt1;
		
		return true;
	}

	function getEvent(uint _id)
		public
		view
		eventExist(_id)
	returns(
		string memory name,
		uint time,
		uint price,
		bool limited,
		uint seats,
		address owner
	) {
		Event memory _event = events[_id];
		return(
			_event.name,
			_event.time,
			_event.price,
			_event.limited,
			_event.seats,
			_event.owner
		);
	}

	function getEventsCount() public view returns(uint) {
		return events.length;
	}

	function buyTicket(uint _eventId)
		public
		payable
		eventExist(_eventId)
		goodTime(events[_eventId].time)
		whenNotPaused()
	{
		Event memory _event = events[_eventId];

		if (_event.limited) require(_event.seats > _event.sold);
		
		ERC20(tokenAddress).transferFrom(msg.sender, _event.owner, _event.price);
		
		uint seat = totalTicketsSold + totalSecondarySaleTickets;
		events[_eventId].sold = seat;
        totalTicketsSold++;
        
		Ticket memory _ticket = Ticket({
		    owner: msg.sender,
			event_id: _eventId,
			ticket_id: 0,
			seat: seat,
			price: _event.price,
			avail: "N"
		});

		uint _ticketId = tickets.push(_ticket).sub(1);
	    
	    tickets[_ticketId].ticket_id = ticketid;
        
        _mint(msg.sender, ticketid);
        ticketid = ticketid + 1;
        
	}
	
	
	function buyTicketForSS(uint _eventId, uint totalSeats, uint margin)
		public
		payable
		eventExist(_eventId)
		goodTime(events[_eventId].time)
		whenNotPaused()
	{
		Event memory _event = events[_eventId];
		
		if (_event.limited) require(_event.seats > _event.sold && (_event.seats - _event.sold > totalSeats));
		
		uint totalPrice = totalSeats * _event.price;
		
		ERC20(tokenAddress).transferFrom(msg.sender, _event.owner, totalPrice);
	
        for (uint i=0;i<totalSeats;i++)
        {
        uint seat = totalTicketsSold + totalSecondarySaleTickets;
		events[_eventId].sold = seat;
		totalSecondarySaleTickets++;
		SecondaryTicketSale memory _saleTkt = SecondaryTicketSale({
		    owner: msg.sender,
			eventid: _eventId,
			ticketid: 0,
			price: _event.price + margin,
			seat: seat,
			avail: "N"
		});

		uint _ticketId2 = secondaryticketsale.push(_saleTkt).sub(1);
		
		secondaryticketsale[_ticketId2].ticketid = ssticketid;
		
		ssticketid = ssticketid + 1;        
        }
	}
	
	function buySoldTicket(uint _ticket_id
	)
		public
		payable
	{
	   TicketSale memory _saleTkt = ticketsale[_ticket_id];

 		ERC20(tokenAddress).transferFrom(msg.sender, _saleTkt.owner, _saleTkt.price);
		ticketsale[_ticket_id].owner = msg.sender;
		ticketsale[_ticket_id].avail = "N";
		totalResaleTicketsSold++;
	}
	
	function buySSTicket(uint _eventId, uint ticket_id)    
		public
		payable
	{
		Event memory _event = events[_eventId];
		
		SecondaryTicketSale memory _saleTkt = secondaryticketsale[ticket_id];
		
		ERC20(tokenAddress).transferFrom(msg.sender, _saleTkt.owner, (_saleTkt.price * 90)/100);
		ERC20(tokenAddress).transferFrom(msg.sender, _event.owner, (_saleTkt.price * 10)/100);
		
		secondaryticketsale[ticket_id].owner = msg.sender;
		secondaryticketsale[ticket_id].avail = "N";
		
		totalSecondaryTicketsSold++;
	    //_mint(msg.sender, ticket_id);
	}
	
}
