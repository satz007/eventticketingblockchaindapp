var Events = artifacts.require("Events");
var GenericToken = artifacts.require("GenericToken");

module.exports = function(deployer) {
  deployer.deploy(GenericToken).then(function() {
  return deployer.deploy(Events, GenericToken.address);
        });
};
