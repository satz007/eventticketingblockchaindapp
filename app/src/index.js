import Web3 from "web3";
import genericTokenArtifact from "../../build/contracts/GenericToken.json";
import eventsArtifact from "../../build/contracts/Events.json";

const App = {
  web3: null,
  account1: null,
  meta: null,
  meta1 : null,
  meta2: null,

  start: async function() {
    var { web3 } = this;
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

    try {
      const networkId = await web3.eth.net.getId();
      
      const deployedNetwork1 = genericTokenArtifact.networks[networkId];
      this.meta1 = new web3.eth.Contract(
        genericTokenArtifact.abi,
        deployedNetwork1.address,
      );

      const deployedNetwork2 = eventsArtifact.networks[networkId];
      this.meta2 = new web3.eth.Contract(
        eventsArtifact.abi,
        deployedNetwork2.address,
      );
      
      const accounts = await web3.eth.getAccounts();
      this.account1 = accounts[0]; //Owner
      
      this.getAllSecondarySaleTicket();
      this.getAllResaleTickets();
	
    } catch (error) {
      console.error("Could not connect to contract or chain.");
    }
  },

  createEvent: async function() {
       const name = document.getElementById("name").value;
	var time = document.getElementById("time").value;
	const price = parseInt(document.getElementById("price").value);
	const limited = document.getElementById("limited").value;
	const seats = parseInt(document.getElementById("seats").value);
        const owner = document.getElementById("owner").value;
      
    const { createEvent } = this.meta2.methods;

    try {
      await createEvent(name,Date.parse(time)/1000,price,limited,seats).send({ from: owner,gas:6000000 });
    document.getElementsByClassName("createmessage")[0].innerHTML = "Success";
    } catch (e) {
    document.getElementsByClassName("createmessage")[0].innerHTML = e;
    }
	},
	
  mintToken: async function() {
    const { mint } = this.meta1.methods;
    try {
    await mint().send({ from: this.account1 });
    document.getElementsByClassName("mintmessage")[0].innerHTML = "Success";
    } catch (e) {
    document.getElementsByClassName("mintmessage")[0].innerHTML = e;
    }
  },

  approveToken: async function() {
    
    const app_price = document.getElementById("app_price").value;
    const app_address = document.getElementById("app_address").value;

    const { approve } = this.meta1.methods;
    const { web3 } = this;
    const networkId = await web3.eth.net.getId();
    const deployedNetwork2 = eventsArtifact.networks[networkId];
	try {
    await approve(deployedNetwork2.address,app_price).send({ from: app_address });
    document.getElementsByClassName("appmessage")[0].innerHTML = "Success";
    } catch (e) {
    document.getElementsByClassName("appmessage")[0].innerHTML = e;
    }
	

  },

	
  transferToken: async function() {
		
   const trans_price = document.getElementById("trans_price").value;
   const trans_address = document.getElementById("trans_address").value;

    const { transfer } = this.meta1.methods;
	
	try {
    await transfer(trans_address,trans_price).send({ from: this.account1 });
    document.getElementsByClassName("transmessage")[0].innerHTML = "Success";
    } catch (e) {
    document.getElementsByClassName("transmessage")[0].innerHTML = e;
    }
  
  },
  
  balanceOf : async function() {

   const owner = document.getElementById("owner").value;

    const { balanceOf } = this.meta1.methods;
    const balance = await balanceOf(owner).call();
    const balanceElement = document.getElementsByClassName("balance")[0];
    balanceElement.innerHTML = balance;
  },

  buyTicket: async function() {
    const eventId = parseInt(document.getElementById("eventid").value);
    const address = document.getElementById("address").value; 
    const price = parseInt(document.getElementById("price").value);
	
    const { approve } = this.meta1.methods;
    const { web3 } = this;
    const networkId = await web3.eth.net.getId();
    const deployedNetwork2 = eventsArtifact.networks[networkId];
    await approve(deployedNetwork2.address,price).send({ from: address});

    const { buyTicket } = this.meta2.methods;
  try {
       await buyTicket(eventId).send({ from: address, gas:6000000 });
    document.getElementsByClassName("buymessage")[0].innerHTML = "Thanks for Purchasing ticket";
    } catch (e) {
    document.getElementsByClassName("buymessage")[0].innerHTML = e;
    }
  },
		
  sellTicket: async function() {
    const eventId = parseInt(document.getElementById("eventid").value);
    const ticketId = parseInt(document.getElementById("ticketid").value);
    const price = parseInt(document.getElementById("price").value);
    const address = document.getElementById("address").value;
    const { SellTicket } = this.meta2.methods;
    try {
    await SellTicket(eventId,ticketId,price).send({ from: address ,gas:6000000});
	document.getElementsByClassName("sellmessage")[0].innerHTML = "Ticket Posted for sale";
    } catch (e) {
    document.getElementsByClassName("sellmessage")[0].innerHTML = e;
    }
  },
	
  buySoldTicket: async function() {
	
    const eventid = parseInt(document.getElementById("eventid").value);
    const saleticketid = parseInt(document.getElementById("saleticketid").value);
    const address = document.getElementById("address").value;
    const owner = document.getElementById("owner").value;
    const resaleprice = parseInt(document.getElementById("resaleprice").value);
	
    const { approve } = this.meta1.methods;
    const { web3 } = this;
    const networkId = await web3.eth.net.getId();
    const deployedNetwork2 = eventsArtifact.networks[networkId];
    await approve(deployedNetwork2.address,resaleprice).send({ from: address });
   
    /* const { approve1 } = this.meta2.methods;
    await approve1(address,saleticketid).send({ from: owner });

    const { transferFrom } = this.meta2.methods;
    await transferFrom(owner,address,resaleticket).send({ from: owner }); */
 
   const { buySoldTicket } = this.meta2.methods;
    try {
      await buySoldTicket(saleticketid).send({ from: address });
      document.getElementsByClassName("buysoldmessage")[0].innerHTML = "Thanks for purchasing ticket";
    } catch (e) {
    document.getElementsByClassName("buysoldmessage")[0].innerHTML = e;
    }
 },

  buyTicketForSS: async function() {
	
    const eventid = parseInt(document.getElementById("eventid").value);
    const seats = parseInt(document.getElementById("totalseats").value);
    const margin = parseInt(document.getElementById("margin").value);
    const address = document.getElementById("address").value;
    const price = parseInt(document.getElementById("price").value);
    console.log("total",price*seats);

    const { approve } = this.meta1.methods;
    const { web3 } = this;
    const networkId = await web3.eth.net.getId();
    const deployedNetwork2 = eventsArtifact.networks[networkId];
    await approve(deployedNetwork2.address,price*seats).send({ from: address});

    const { buyTicketForSS } = this.meta2.methods;
	try {
    await buyTicketForSS(eventid,seats,margin).send({ from: address,gas: 6000000});
	document.getElementsByClassName("buyforSSmessage")[0].innerHTML = "Thanks for purchasing ticket";
    } catch (e) {
    document.getElementsByClassName("buyforSSmessage")[0].innerHTML = e;
    }
    
	
 },
 
 updateSSTicketPrice: async function() {
	
    const ticketid = parseInt(document.getElementById("ticketid").value);
    const price = parseInt(document.getElementById("newprice").value);
    const owner = document.getElementById("owner").value;
	
    const { updateSSTicketPrice } = this.meta2.methods;
    try {
    await updateSSTicketPrice(ticketid,price).send({ from: owner });
    document.getElementsByClassName("updatemessage")[0].innerHTML = "Updated Successfully";
    } catch (e) {
    document.getElementsByClassName("updatemessage")[0].innerHTML = e;
    }

 },
 
 buySSTicket: async function() {
	
    const eventid = parseInt(document.getElementById("eventid").value);
    const saleticketid = parseInt(document.getElementById("saleticketid").value);
    const owner = document.getElementById("owner").value;
    const address = document.getElementById("address").value;
    const resaleprice = parseInt(document.getElementById("resaleprice").value); 
	
    const { approve } = this.meta1.methods;
    const { web3 } = this;
    const networkId = await web3.eth.net.getId();
    const deployedNetwork2 = eventsArtifact.networks[networkId];
    await approve(deployedNetwork2.address,resaleprice).send({ from: address});

    const { buySSTicket } = this.meta2.methods;
    
	try {
    await buySSTicket(eventid,saleticketid).send({ from: address,gas: 6000000});
	document.getElementsByClassName("buySSmessage")[0].innerHTML = "Thanks for purchasing ticket";
    } catch (e) {
    document.getElementsByClassName("buySSmessage")[0].innerHTML = e;
    }
 },

 getAllAvailableTickets: async function() {

    const { getAllAvailableTickets } = this.meta2.methods;
    var res = await getAllAvailableTickets().call();
    var array = [res[0],res[1],res[2],res[3],res[4]];
    table = document.getElementById("table");
    
    res[0].unshift("ID")
    res[1].unshift("TICKET ID")
    res[2].unshift("SEAT")
    res[3].unshift("PRICE")
    res[4].unshift("OWNER")
        
            for(var i = 0; i < array.length; i++)
           {
               var newRow = table.insertRow(table.length);
               for(var j = 0; j < array[i].length; j++)
               {
                   var cell = newRow.insertCell(j);
                   cell.innerHTML = array[i][j];
               }
           }
 },

 getAllResaleTickets: async function() {
    
    const { getAllResaleTickets } = this.meta2.methods;
    var res = await getAllResaleTickets().call();

    var array = [res[0],res[1],res[2],res[3],res[4]];
    let table = document.getElementById("resaletable");
    
    res[0].unshift("ID")
    res[1].unshift("TICKET ID")
    res[2].unshift("SEAT")
    res[3].unshift("PRICE")
    res[4].unshift("OWNER")

        for(var i = 0; i < array.length; i++)
           {
               var newRow = table.insertRow(table.length);
               for(var j = 0; j < array[i].length; j++)
               {
                   var cell = newRow.insertCell(j);
                   cell.innerHTML = array[i][j];
               }
           }

 },

 getAllSecondarySaleTicket: async function() {

    const { getAllSecondarySaleTicket } = this.meta2.methods;
    var res = await getAllSecondarySaleTicket().call();
    
    var array = [res[0],res[1],res[2],res[3],res[4]];
    res[0].unshift("ID")
    res[1].unshift("TICKET ID")
    res[2].unshift("SEAT")
    res[3].unshift("PRICE")
    res[4].unshift("OWNER")
    let table = document.getElementById("sstable");
    
	for(var i = 0; i < array.length; i++)
           {
               var newRow = table.insertRow(table.length);
               for(var j = 0; j < array[i].length; j++)
               {
                   var cell = newRow.insertCell(j);
                   cell.innerHTML = array[i][j];
               }
	}
 },

};

window.App = App;

window.addEventListener("load", function() {
  if (window.ethereum) {

    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable();
  } else {
    console.warn(
      "No web3 detected. Using http://127.0.0.1:9545",
    );
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
    );
  }

  App.start();
});

